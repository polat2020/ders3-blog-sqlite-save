package com.polat.blog.Activity;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;

import com.polat.blog.Db.DatabaseHelper;
import com.polat.blog.Dto.BlogItemDto;
import com.polat.blog.databinding.ActivityDetailsViewBinding;

import java.util.ArrayList;

public class DetailsViewActivity extends AppCompatActivity {

    private ActivityDetailsViewBinding binding;

    DatabaseHelper db;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityDetailsViewBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        db = new DatabaseHelper(this);


        Intent intent = getIntent();
        Integer id = intent.getIntExtra("id", 0);

        try {

            BlogItemDto item = db.getir(id);


            binding.blogNameText.setText(item.getName());
            binding.detailText.setText(item.getDetail());
            Bitmap bitmap = BitmapFactory.decodeByteArray(item.getImage(), 0, item.getImage().length);
            binding.imageView.setImageBitmap(bitmap);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }


}
