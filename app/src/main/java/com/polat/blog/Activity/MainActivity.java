package com.polat.blog.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.polat.blog.Adapters.BlogItemAdapter;
import com.polat.blog.Db.DatabaseHelper;
import com.polat.blog.Dto.BlogItemDto;
import com.polat.blog.R;
import com.polat.blog.databinding.ActivityMainBinding;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    BlogItemAdapter blogItemAdapter;
    DatabaseHelper db;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        db = new DatabaseHelper(this);
        getData();

    }

    public void getData() {
        try {
            ArrayList<BlogItemDto> list = db.getirList();
            blogItemAdapter = new BlogItemAdapter(list);
            binding.recyclerView.setAdapter(blogItemAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Inflater
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.blog_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        if (item.getItemId() == R.id.menu_blog_ekle) {
            Intent intent = new Intent(MainActivity.this, DetailsActivity.class);
            startActivity(intent);
        }

        return super.onOptionsItemSelected(item);
    }
}
