package com.polat.blog.Adapters;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.polat.blog.Activity.DetailsViewActivity;
import com.polat.blog.Dto.BlogItemDto;
import com.polat.blog.Holders.BlogItemHolder;
import com.polat.blog.databinding.RecyclerRowBinding;

import java.util.ArrayList;

public class BlogItemAdapter extends RecyclerView.Adapter<BlogItemHolder> {

    ArrayList<BlogItemDto> blogItemArrayList;

    public BlogItemAdapter(ArrayList<BlogItemDto> blogItemArrayList) {
        this.blogItemArrayList = blogItemArrayList;
    }

    @Override
    public BlogItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerRowBinding binding =
                RecyclerRowBinding.inflate(LayoutInflater.from(parent.getContext()), parent, false);
        return new BlogItemHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull BlogItemHolder holder, int position) {
        BlogItemDto item = blogItemArrayList.get(position);
        holder.binding.nameText.setText(item.getName());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(holder.itemView.getContext(), DetailsViewActivity.class);
                intent.putExtra("id", item.getId());
                holder.itemView.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return blogItemArrayList.size();
    }


}
